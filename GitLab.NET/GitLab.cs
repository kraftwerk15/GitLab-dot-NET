﻿using System;
using System.Web;
using Newtonsoft.Json;
using RestSharp;

namespace GitLabDotNet
{
    /// <summary>
    /// Class for interfacing with a GitLab API via .NET
    /// See https://gitlab.com/help/api/README.md for API Reference
    /// </summary>
    public partial class GitLab
    {
        public Config CurrentConfig = null;

        public GitLab(Config _config)
        {
            CurrentConfig = _config;
            Refresh();   
        }

        /// <summary>
        /// Refreshes Everything for this GitLab object;
        /// </summary>
        public void Refresh()
        {
            RefreshProjects();
            RefreshGroups();
            RefreshUsers();
            RefreshCurrentUser();
        }

        /// <summary>
        /// Login to the specified GitLab URI.
        /// </summary>
        /// <param name="_URI">The _ URI.</param>
        /// <param name="_Login">The _ login.</param>
        /// <param name="_Password">The _ password.</param>
        /// <param name="_LoginIsEmail">if set to <c>true</c> [_ login is email].</param>
        /// <returns>GitLab object with a config set up for the logged in user.</returns>
        /// <exception cref="GitLab.GitLabServerErrorException"></exception>
        public static GitLab Login(string _URI, string _Login, string _Password, bool _LoginIsEmail = false)
        {
            Config C = new Config {GitLabURI = _URI};


            try
            {
                 string URI = C.APIUrl + "session"                    
                    + "?password=" + HttpUtility.UrlEncode(_Password);

                if (!_LoginIsEmail)
                    URI += "&login=" + HttpUtility.UrlEncode(_Login);
                else
                    URI += "&email=" + HttpUtility.UrlEncode(_Login);

                RestClient client = new RestClient(URI)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", C.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }

                User U = JsonConvert.DeserializeObject<User>(response.Content);
                C.APIKey = U.private_token;
                return new GitLab(C);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
