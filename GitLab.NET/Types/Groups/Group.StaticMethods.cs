﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GitLabDotNet
{
    partial class GitLab
    {
        public partial class Group
        {
            /// <summary>
            /// Creates a new project group. Available only for users who can create groups.
            /// </summary>
            /// <param name="_Config"></param>
            /// <param name="_Name"></param>
            /// <param name="_Path"></param>
            /// <param name="_Description"></param>
            /// <returns></returns>
            public static Group Create(Config _Config, string _Name, string _Path = null, string _Description = null)
            {
                if (_Path == null)
                    _Path = _Name.Trim().Replace(" ", "-").ToLowerInvariant();

                string URI = _Config.APIUrl + "groups?name=" + HttpUtility.UrlEncode(_Name)
                                            + "&path=" + HttpUtility.UrlEncode(_Path);

                if (_Description != null)
                    URI += "&description=" + HttpUtility.UrlEncode(_Description);

                RestClient client = new RestClient(URI)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", _Config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }

                Group RetVal = JsonConvert.DeserializeObject<Group>(response.Content);
                return RetVal;

            }

            /// <summary>
            /// Get a list of groups. (As user: my groups, as admin: all groups)
            /// </summary>
            /// <param name="_Config"></param>
            /// <returns></returns>
            public static List<Group> List(Config _Config, string _Search = null)
            {
                List<Group> RetVal = new List<Group>();

                try
                {
                    int page = 1;
                    List<Group> groups = new List<Group>();

                    do
                    {
                        groups.Clear();

                        string URI = (_Config.APIUrl + "groups/"
                                + "?per_page=100"
                                + "&page=" + page.ToString());

                        if (_Search != null)
                            URI += "&search=" + HttpUtility.UrlEncode(_Search);

                        RestClient client = new RestClient(URI)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", _Config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }

                        
                        dynamic Result = JsonConvert.DeserializeObject(response.Content);
                        if (Result is JArray)
                        {
                            JArray ResultArray = (JArray)Result;
                            foreach (JToken Token in ResultArray)
                            {
                                Group G = JsonConvert.DeserializeObject<Group>(Token.ToString());
                                groups.Add(G);
                            }
                        }
                        
                        page++;
                        RetVal.AddRange(groups);
                    }
                    while (groups.Count > 0 & page < 100);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RetVal;
            }

            /// <summary>
            /// Get a single group description by numeric group ID
            /// </summary>
            /// <param name="_Config"></param>
            /// <param name="_id"></param>
            /// <returns></returns>
            public static Group Get(Config _Config, int _id)
            {
                Group RetVal = null;

                try
                {
                    string URI = _Config.APIUrl + "groups/" + _id.ToString();

                    RestClient client = new RestClient(URI)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.GET);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", _Config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    
                    RetVal = JsonConvert.DeserializeObject<Group>(response.Content);

                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RetVal;
            }


            /// <summary>
            /// Removes group with all projects inside.
            /// </summary>
            /// <param name="_Config"></param>
            /// <param name="_Group"></param>
            public static void Delete(Config _Config, Group _Group)
            {
                RestClient client = new RestClient(_Config.APIUrl + "groups/" + _Group.id.ToString())
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.DELETE);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", _Config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
            }

            /// <summary>
            /// Get a list of group members viewable by the authenticated user.
            /// </summary>
            /// <param name="_Config"></param>
            /// <param name="_Group"></param>
            /// <returns></returns>
            public static List<Member> ListMembers(Config _Config, Group _Group)
            {
                List<Member> RetVal = new List<Member>();

                try
                {
                    int page = 1;
                    List<Member> members = new List<Member>();

                    do
                    {
                        members.Clear();

                        string URI = (_Config.APIUrl + "groups/"
                                + "?per_page=100"
                                + "&page=" + page.ToString());

                        RestClient client = new RestClient(URI)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", _Config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }

                        dynamic result = JsonConvert.DeserializeObject(response.Content);
                        if (result is JArray)
                        {
                            JArray resultArray = (JArray)result;
                            foreach (JToken token in resultArray)
                            {
                                Member m = JsonConvert.DeserializeObject<Member>(token.ToString());
                                members.Add(m);
                            }
                        }
                        
                        page++;
                        RetVal.AddRange(members);
                    }
                    while (members.Count > 0 & page < 100);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RetVal;
            }

            /// <summary>
            /// Adds a user to the list of group members.
            /// </summary>
            /// <param name="config"></param>
            /// <param name="group"></param>
            /// <param name="user"></param>
            /// <param name="accessLevel"></param>
            public static void AddMember(Config config, Group group, User user, Member.AccessLevel accessLevel)
            {
                string uri = config.APIUrl + "groups/" + group.id.ToString() + "/members/?user_id=" + user.id +
                             "&access_level=" + Convert.ToInt64(accessLevel);

                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
            }

            /// <summary>
            /// Updates a group team member to a specified access level.
            /// </summary>
            /// <param name="config"></param>
            /// <param name="group"></param>
            /// <param name="user"></param>
            /// <param name="accessLevel"></param>
            public static void UpdateMember(Config config, Group group, User user, Member.AccessLevel accessLevel)
            {
                string uri = config.APIUrl + "groups/" + group.id.ToString() + "/members/" + user.id +
                             "&access_level=" + Convert.ToInt64(accessLevel);

                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.PUT);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
            }

            /// <summary>
            /// Removes user from group.
            /// </summary>
            /// <param name="config"></param>
            /// <param name="group"></param>
            /// <param name="user"></param>
            public static void DeleteMember(Config config, Group group, User user)
            {
                string uri = config.APIUrl + "groups/" + group.id.ToString() + "/members/" + user.id;

                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.DELETE);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
            }

            /// <summary>
            /// Transfer a project to the Group namespace. Available only for admin
            /// </summary>
            /// <param name="config"></param>
            /// <param name="group">Group to transfer project to</param>
            /// <param name="project">Project to be transferred</param>
            // ReSharper disable once UnusedMember.Global
            public static void TransferProject(Config config, Group group, Project project)
            {
                string uri = config.APIUrl + "groups/" + group.id.ToString() + "/projects/" + project.id;
                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
            }
        }
    }
}

