﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
//using unirest_net.http;
using System.Web;
using RestSharp;


namespace GitLabDotNet
{
    public partial class GitLab
    {
        public class Namespace : object
        {
            public int id;
            public string path, kind;

            public override string ToString()
            {
                return path;
            }

            public static List<Namespace> List(Config _Config, bool _OnlyOwned = true, string _Search = null)
            {

                List<Namespace> RetVal = new List<Namespace>();

                try
                {
                    int page = 1;

                    RestClient client = new RestClient(_Config.APIUrl + "user")
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.GET);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", _Config.APIKey);
                    IRestResponse response = client.Execute(request);

                    User me = JsonConvert.DeserializeObject<User>(response.Content);

                    List<Namespace> namespaces = new List<Namespace>();

                    do
                    {
                        namespaces.Clear();

                        string URI = _Config.APIUrl + "/namespaces?per_page=100"
                                + "&page=" + page.ToString();

                        if (_Search != null)
                            URI += "%&search=" + HttpUtility.UrlEncode(_Search);

                        RestClient doClient = new RestClient(URI)
                        {
                            Timeout = -1
                        };
                        RestRequest doRequest = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", _Config.APIKey);
                        IRestResponse doResponse = doClient.Execute(doRequest);

                        int responseCode = Convert.ToInt32(doResponse.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }

                        namespaces = JsonConvert.DeserializeObject<List<Namespace>>(doResponse.Content);

                        foreach (Namespace NS in namespaces)
                        {
                            if ((me.username.ToUpperInvariant() == NS.path.ToUpperInvariant()
                                    && NS.kind.ToUpperInvariant() == "USER")
                                    || NS.kind.ToUpperInvariant() == "GROUP"
                                    || !_OnlyOwned
                                    )
                            {
                                RetVal.Add(NS);
                            }
                        }

                        page++;
                        RetVal.AddRange(namespaces);
                    }
                    while (namespaces.Count > 0 & page < 100);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RetVal;
            }
        }
    }
}

