﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GitLabDotNet
{
    public partial class GitLab
    {
        public partial class Project
        {
            public class Issue
            {
                public int id, iid, project_id;
                public string[] labels;
                public Milestone milestone;
                public User asignee;
                public User author;
                public string title, description, state, updated_at, created_at;

                public enum StateEvent
                {
                    None, Close, Reopen
                }

                /// <summary>
                /// Lists all Issues for a given project or the current user if _Project is null
                /// </summary>
                /// <param name="_Config">The configuration.</param>
                /// <param name="_Project">The project.</param>
                /// <returns></returns>
                public static List<Issue> List(Config _Config, Project _Project = null, Milestone _Milestone = null)
                {
                    List<Issue> RetVal = new List<Issue>();

                    try
                    {
                        int page = 0
                            ;
                        List<Issue> issues = new List<Issue>();

                        do
                        {
                            issues.Clear();

                            string uri = _Config.APIUrl;
                            if(_Milestone != null)
                                uri += "/projects/" + _Milestone.project_id.ToString() + "/milestones/" +_Milestone.id+ "/issues";
                            else if (_Project != null)
                                uri += "/projects/" + _Project.id.ToString() + "/issues";
                            else
                                uri += "/issues";

                            uri += "?per_page=100"
                                    + "&page=" + page.ToString();

                            RestClient client = new RestClient(uri)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.GET);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", _Config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }
                            dynamic result = JsonConvert.DeserializeObject(response.Content);
                            if (result is JArray)
                            {
                                JArray resultArray = (JArray)result;
                                issues.AddRange(resultArray.Select(token =>
                                    JsonConvert.DeserializeObject<Issue>(token.ToString())));
                            }
                            

                            page++;
                            RetVal.AddRange(issues);
                        }
                        while (issues.Count > 0 & page < 100);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return RetVal;
                }

                /// <summary>
                /// Gets the specified Issue by numeric ID.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="project">The _ project.</param>
                /// <param name="id">The _ identifier.</param>
                /// <returns></returns>
                // ReSharper disable once UnusedMember.Global
                public static Issue Get(Config config, Project project, int id)
                {
                    string uri = config.APIUrl + "projects/" + project.id.ToString() + "/issues/" + id.ToString();
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.GET);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Issue>(response.Content);
                    
                }

                // ReSharper disable once UnusedMember.Global
                public static Issue Add(Config config, Project project, Issue issue)
                {
                    string labels = null;
                    bool first = true;

                    if (issue.labels != null)
                    {
                        labels = "";

                        foreach (string l in issue.labels)
                        {
                            if (!first)
                                labels += ",";

                            labels += l;
                            first = false;
                        }
                    }

                    string uri = config.APIUrl + "projects/" + project.id.ToString() + "/issues?"
                        + "title=" + HttpUtility.UrlEncode(issue.title)
                        + "&description=" + HttpUtility.UrlEncode(issue.description);

                    if (issue.asignee != null)
                        uri += "&assignee_id=" + issue.asignee.id;
                    if (issue.milestone != null)
                        uri += "&milestone_id=" + issue.milestone.id;
                    if (issue.labels != null)
                        uri += "&labels=" + HttpUtility.UrlEncode(labels);

                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.POST);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Issue>(response.Content);
                    
                }


                /// <summary>
                /// Saves the specified Issue on the GitLab server
                /// </summary>
                /// <param name="config">The configuration.</param>
                /// <param name="issue">The issue.</param>
                /// <param name="stateEvent"></param>
                /// <returns></returns>
                // ReSharper disable once UnusedMember.Global
                public static Issue Update(Config config, Issue issue, StateEvent stateEvent = StateEvent.None)
                {
                    string labels = null;
                    bool first = true;

                    if (issue.labels != null)
                    {
                        labels = "";

                        foreach (string l in issue.labels)
                        {
                            if (!first)
                                labels += ",";

                            labels += l;
                            first = false;
                        }
                    }

                    string uri = config.APIUrl + "projects/" + issue.project_id.ToString() + "/issues/" + issue.id.ToString()
                        + "?title=" + HttpUtility.UrlEncode(issue.title)
                        + "&description=" + HttpUtility.UrlEncode(issue.description);
                    if (issue.asignee != null)
                        uri += "&assignee_id=" + issue.asignee.id;
                    if (issue.milestone != null)
                        uri += "&milestone_id=" + issue.milestone.id;
                    if (issue.labels != null)
                        uri += "&labels=" + HttpUtility.UrlEncode(labels);
                    if (stateEvent != StateEvent.None)
                        uri += "&state_event=" + HttpUtility.UrlEncode(stateEvent.ToString().ToLowerInvariant());

                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.PUT);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Issue>(response.Content);
                }

                /// <summary>
                /// Lists the comments.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="issue">The _ issue.</param>
                /// <returns></returns>
                // ReSharper disable once UnusedMember.Global
                public static List<Note> ListComments(Config config, Issue issue)
                {
                    List<Note> retVal = new List<Note>();

                    try
                    {
                        int page = 1;
                        List<Note> notes = new List<Note>();

                        do
                        {
                            notes.Clear();

                            string uri = config.APIUrl;

                            uri += "projects/" + issue.project_id.ToString() + "/issues/" + issue.id.ToString() + "/notes";

                            uri += "?per_page=100"
                                   + "&page=" + page.ToString();
                            RestClient client = new RestClient(uri)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.GET);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }
                            dynamic Result = JsonConvert.DeserializeObject(response.Content);
                            if (Result is JArray)
                            {
                                JArray ResultArray = (JArray)Result;
                                foreach (JToken Token in ResultArray)
                                {
                                    Note N = JsonConvert.DeserializeObject<Note>(Token.ToString());
                                    notes.Add(N);
                                }
                            }

                            page++;
                            retVal.AddRange(notes);
                        }
                        while (notes.Count > 0 & page < 100);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;

                }

                /// <summary>
                /// Gets a comment by ID.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="issue">The _ issue.</param>
                /// <param name="id">The _ identifier.</param>
                /// <returns></returns>
                // ReSharper disable once UnusedMember.Global
                public static Note GetComment(Config config, Issue issue, int id)
                {
                    string uri = config.APIUrl + "projects/" + issue.project_id.ToString() + "/issues/" + issue.id.ToString() + "/notes/" + id.ToString();

                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.GET);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                }

                /// <summary>
                /// Updates a comment.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="issue">The _ issue.</param>
                /// <param name="note">The _ note.</param>
                /// <returns></returns>
                // ReSharper disable once UnusedMember.Global
                public static Note UpdateComment(Config config, Issue issue, Note note)
                {
                    string uri = config.APIUrl + "projects/" + issue.project_id.ToString() + "/issues/" + issue.id + "/notes/" + note.id
                        + "?body=" + HttpUtility.UrlEncode(note.body);

                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.PUT);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                    
                }

                /// <summary>
                /// Adds a comment.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="issue">The _ issue.</param>
                /// <param name="body">The _ body.</param>
                /// <returns></returns>
                // ReSharper disable once UnusedMember.Global
                public static Note AddComment(Config config, Issue issue, string body)
                {
                    string uri = config.APIUrl + "projects/" + issue.project_id.ToString() + "/issues/" + issue.id + "/notes?"
                        + "body=" + HttpUtility.UrlEncode(body);
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.POST);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                    
                }

            }
        }
    }
}
