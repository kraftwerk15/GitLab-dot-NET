﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GitLabDotNet
{
    public partial class GitLab
    {
        public partial class Project
        {
            private LabelList _Labels;

            public LabelList Labels
            {
                get
                {
                    if (_Labels == null)
                        _Labels = new LabelList(this);

                    return _Labels;
                }

            }
            
            public class Label
            {
                public string Name { get; set; }
                public string Color { get; set; }
                private Project Parent;

                /// <summary>
                /// Sets the parent project.
                /// </summary>
                /// <param name="_Project">The project.</param>
                internal void SetParent(Project _Project)
                {
                    Parent = _Project;
                }

                /// <summary>
                /// Updates this instance.
                /// </summary>
                /// <exception cref="GitLabStaticAccessException">No parent project for operation</exception>
                public void Update()
                {
                    if (Parent != null)
                    {
                        Label.Update(Parent.Parent.CurrentConfig, Parent, this, this.Name, this.Color);
                    }
                    else
                        throw new GitLabStaticAccessException("No parent project for operation");
                }

                /// <summary>
                /// List all labels associated with a project
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <returns></returns>
                public static List<Label> List(Config config, Project project)
                {
                    List<Label> retVal = new List<Label>();

                    try
                    {
                        int page = 1;
                        List<Label> labels = new List<Label>();

                        do
                        {
                            labels.Clear();

                            string uri = (config.APIUrl + "projects/" + project.id.ToString() + "/labels"
                                    + "?per_page=100"
                                    + "&page=" + page.ToString());

                            RestClient client = new RestClient(uri)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.GET);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }

                            dynamic result = JsonConvert.DeserializeObject(response.Content);
                            if (result is JArray)
                            {
                                JArray resultArray = (JArray)result;
                                labels.AddRange(resultArray.Select(token =>
                                    JsonConvert.DeserializeObject<Label>(token.ToString())));
                            }
                            

                            page++;
                            retVal.AddRange(labels);
                        }
                        while (labels.Count > 0 & page < 100);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                /// <summary>
                /// Add a label to a project.
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <param name="name"></param>
                /// <param name="color">A String representing a color in #FFFFFF format</param>
                /// <returns></returns>
                public static Label Add(Config config, Project project, string name, string color)
                {
                    string uri = config.APIUrl + "projects/" + project.id.ToString() + "/labels/"
                        + "?name=" + HttpUtility.UrlEncode(name)
                        + "&color=" + HttpUtility.UrlEncode(color);
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.POST);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Label>(response.Content);
                }

                /// <summary>
                /// Update a Project Label. At least one of NewName or NewColor must be specified
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <param name="label"></param>
                /// <param name="newName"></param>
                /// <param name="newColor"></param>
                /// <returns></returns>
                public static Label Update(Config config, Project project, Label label, string newName = null,
                    string newColor = null)
                {
                    if (newName == null && newColor == null)
                        throw new ArgumentException("Color or New Name must be specified.");

                    string uri = config.APIUrl + "projects/" + project.id.ToString() + "/labels/"
                        + "?name=" + HttpUtility.UrlEncode(label.Name);

                    if (newName != null)
                        uri += "&new_name=" + HttpUtility.UrlEncode(newName);
                    
                    if(newColor != null )
                        uri += "&color=" + HttpUtility.UrlEncode(newColor);
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.PUT);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Label>(response.Content);
                    
                }

                /// <summary>
                /// Deletes a label from a project. API Function request is by name, label has no ID field.
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <param name="label"></param>
                public static void Delete(Config config, Project project, Label label)
                {
                    string uri = config.APIUrl + "projects/" + project.id.ToString() + "/labels/"
                        + "?name=" + HttpUtility.UrlEncode(label.Name) ;
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.POST);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                }
            }

            public class LabelList: List<Label>
            {
                private Project Parent;

                public LabelList(Project _Parent)
                {
                    Parent = _Parent;
                }

                /// <summary>
                /// Refreshes the items in this list from the server
                /// </summary>
                /// <exception cref="GitLabStaticAccessException">No parent project available for operation.</exception>
                public void RefreshItems()
                {
                    if (Parent != null)
                    {
                        this.Clear();
                        foreach (Label L in Label.List(Parent.Parent.CurrentConfig, Parent))
                        {
                            base.Add(L);
                            L.SetParent(Parent);
                        }
                    }
                    else
                        throw new GitLabStaticAccessException("No parent project available for operation.");
                }

                /// <summary>
                /// Adds the specified label to the parent project.
                /// </summary>
                /// <param name="_Label">The label.</param>
                /// <exception cref="GitLabStaticAccessException">No parent project available for operation.</exception>
                new public void Add(Label _Label)
                {
                    if (Parent != null)
                    {
                        Label.Add(Parent.Parent.CurrentConfig, Parent, _Label.Name, _Label.Color);
                        RefreshItems();
                        
                    }
                    else
                        throw new GitLabStaticAccessException("No parent project available for operation.");
                }

                /// <summary>
                /// Removes the specified label.
                /// </summary>
                /// <param name="_Label">The label.</param>
                /// <exception cref="GitLabStaticAccessException">No parent project available for operation.</exception>
                new public void Remove(Label _Label)
                {
                    if (Parent != null)
                    {
                        Label.Delete(Parent.Parent.CurrentConfig, Parent, _Label);
                        RefreshItems();
                    }
                    else
                        throw new GitLabStaticAccessException("No parent project available for operation.");
                }
            }
        }
    }
}
