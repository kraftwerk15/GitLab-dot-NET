﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GitLabDotNet
{
    public partial class GitLab
    {
        public partial class Project
        {
            public class MergeRequest
            {

                private Project Parent;               

                internal void SetParent(Project _Project)
                {
                    Parent = _Project;
                }

                /// <summary>
                /// Accepts the Merge Request.
                /// </summary>
                /// <param name="CommitMessage">The commit message.</param>
                public void Accept(string CommitMessage = null)
                {
                    if (Parent != null)
                    {                        
                        MergeRequest.Accept(Parent.Parent.CurrentConfig, this, CommitMessage);
                    }
                    else
                        throw new GitLabStaticAccessException("Unable to complete operation without parent project");
                }

                public void Update(StateEvent _NewState = StateEvent.None)
                {
                    if (Parent != null)
                    {
                        MergeRequest.Update(Parent.Parent.CurrentConfig, this, this.target_branch, this.title, this.description, this.assignee, labels, _NewState );
                    }
                    else
                        throw new GitLabStaticAccessException("Unable to complete operation without parent project");
                }

                public int id, iid, project_id, source_project_id, target_project_id = -1;
                /// <summary>
                /// With GitLab 8.2 the return fields upvotes and downvotes are deprecated and always return 0 
                /// </summary>
                public int upvotes;
                /// <summary>
                /// With GitLab 8.2 the return fields upvotes and downvotes are deprecated and always return 0
                /// </summary>
                public int downvotes;
                public string target_branch, source_branch, title, state, description;
                public string[] labels;
                public Milestone milestone;
                public Repository.Diff[] files;
                public User author;
                public User assignee;
                public bool work_in_progress;
                public int user_notes_count;

                public enum StateEvent
                {
                    None, Close, Reopen, Merge
                }

                /// <summary>
                /// List all merge requests
                /// </summary>
                /// <param name="_Config"></param>
                /// <param name="_Project"></param>
                /// <returns></returns>
                public static List<MergeRequest> List(Config _Config, Project _Project)
                {
                    List<MergeRequest> RetVal = new List<MergeRequest>();

                    try
                    {
                        int page = 1;
                        List<MergeRequest> mergerequests = new List<MergeRequest>();

                        do
                        {
                            mergerequests.Clear();

                            string URI = _Config.APIUrl + "projects/" + _Project.id + "/merge_requests";
                            URI += "?per_page=100" + "&page=" + page.ToString();

                            RestClient client = new RestClient(URI)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.GET);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", _Config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }
                            dynamic result = JsonConvert.DeserializeObject(response.Content);
                            if (result is JArray)
                            {
                                JArray resultArray = (JArray)result;
                                mergerequests.AddRange(resultArray.Select(token =>
                                    JsonConvert.DeserializeObject<MergeRequest>(token.ToString())));
                            }
                            
                            page++;
                            RetVal.AddRange(mergerequests);
                        }
                        while (mergerequests.Count > 0 & page < 100);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return RetVal;
                }

                /// <summary>
                /// 
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <param name="id"></param>
                /// <param name="withChanges">Get MergeRequest with associated changes. See https://gitlab.com/help/api/merge_requests.md#get-single-mr-changes</param>
                /// <returns></returns>
                public static MergeRequest Get(Config config, Project project, int id, bool withChanges = false)
                {
                    MergeRequest retVal;
                    try
                    {
                        string uri = config.APIUrl + "/projects/" + project.id + "/merge_requests/" + id.ToString();

                        if (withChanges)
                            uri += "/changes";
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        retVal = JsonConvert.DeserializeObject<MergeRequest>(response.Content);
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                /// <summary>
                /// Creates a new merge request.
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <param name="sourceBranch">The source branch</param>
                /// <param name="targetBranch">The target branch</param>
                /// <param name="_Title">Title of MR</param>
                /// <param name="description">Description of MR</param>
                /// <param name="assignee">Assignee user </param>
                /// <param name="targetProject">The target project</param>
                /// <param name="labels">Labels for MR as a comma-separated list</param>
                /// <returns></returns>
                public static MergeRequest Create(Config config, Project project, string sourceBranch, string targetBranch, string _Title
                    , string description=null, User assignee = null, Project targetProject = null, string[] labels=null )
                {
                    MergeRequest retVal;
                    try
                    {
                        string _labels = "";
                        bool first = true;

                        foreach (string l in labels)
                        {
                            if (!first)
                                _labels += ",";

                            _labels += l;
                            first = false;
                        }

                        string uri = config.APIUrl + "/projects/" + project.id + "/merge_requests"
                            + "?source_branch=" + HttpUtility.UrlEncode(sourceBranch)
                            + "&target_branch=" + HttpUtility.UrlEncode(targetBranch)
                            + "&title=" + HttpUtility.UrlEncode(_Title);

                        if (assignee != null)
                            uri += "&assignee_id=" + assignee.id.ToString();
                        if (description != null)
                            uri += "&description=" + HttpUtility.UrlEncode(description);
                        if (targetProject != null)
                            uri += "&target_project_id=" + targetProject.id.ToString();
                        if (labels != null)
                            uri += "&labels=" + HttpUtility.UrlEncode(_labels);
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.POST);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        retVal = JsonConvert.DeserializeObject<MergeRequest>(response.Content);
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                /// <summary>
                /// Update Merge Request
                /// </summary>
                /// <param name="config"></param>
                /// <param name="mergeRequest"></param>
                /// <param name="targetBranch"></param>
                /// <param name="title"></param>
                /// <param name="description"></param>
                /// <param name="assignee"></param>
                /// <param name="labels"></param>
                /// <returns></returns>
                public static MergeRequest Update(Config config, MergeRequest mergeRequest, string targetBranch =null, string title=null
                  , string description = null, User assignee = null, string[] labels = null, StateEvent stateEvent = StateEvent.None)
                {
                    MergeRequest retVal = null;
                    try
                    {
                        bool first = true;
                        string ls = "";

                        if (labels != null)
                        {
                            foreach (string l in labels)
                            {
                                if (!first)
                                    ls += ",";

                                ls += l;
                                first = false;
                            }

                            string uri = config.APIUrl + "/projects/" + mergeRequest.project_id + "/merge_requests/" +
                                         mergeRequest.id + "?";

                            if (targetBranch != null)
                                uri += "&target_branch=" + HttpUtility.UrlEncode(targetBranch);
                            if (title != null)
                                uri += "&title=" + HttpUtility.UrlEncode(title);
                            if (assignee != null)
                                uri += "&assignee_id=" + assignee.id.ToString();
                            if (description != null)
                                uri += "&description=" + HttpUtility.UrlEncode(description);
                            if (labels != null)
                                uri += "&labels=" + HttpUtility.UrlEncode(ls);
                            if (stateEvent != StateEvent.None)
                                uri += "&state_event=" + stateEvent.ToString().ToLowerInvariant();
                            RestClient client = new RestClient(uri)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.PUT);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }

                            retVal = JsonConvert.DeserializeObject<MergeRequest>(response.Content);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                /// <summary>
                /// Accepts the specified MergeRequest.
                /// </summary>
                /// <param name="config">The configuration.</param>
                /// <param name="mergeRequest">The merge request.</param>
                /// <param name="commitMessage">The commit message (Optional).</param>
                /// <returns></returns>
                /// <exception cref="GitLabServerErrorException"></exception>
                public static MergeRequest Accept(Config config, MergeRequest mergeRequest, string commitMessage = null)
                {
                    MergeRequest retVal;
                    try
                    {
                        string uri = config.APIUrl + "/projects/" + mergeRequest.project_id + "/merge_requests/" + mergeRequest.id + "/merge?";

                        if (commitMessage != null)
                            uri += "&merge_commit_message=" + HttpUtility.UrlEncode(commitMessage);
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.PUT);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        retVal = JsonConvert.DeserializeObject<MergeRequest>(response.Content);
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }


                /// <summary>
                /// Lists the comments.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="mergeRequest"></param>
                /// <returns></returns>
                public static List<Note> ListComments(Config config, MergeRequest mergeRequest)
                {
                    List<Note> retVal = new List<Note>();

                    try
                    {
                        int page = 1;
                        List<Note> notes = new List<Note>();

                        do
                        {
                            notes.Clear();

                            string URI = config.APIUrl;

                            URI += "projects/" + mergeRequest.project_id.ToString() + "/merge_requests/" + mergeRequest.id.ToString() + "/notes";


                            URI += "?per_page=100"
                                    + "&page=" + page.ToString();

                            RestClient client = new RestClient(URI)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.GET);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }
                            dynamic result = JsonConvert.DeserializeObject(response.Content);
                            if (result is JArray)
                            {
                                JArray resultArray = (JArray)result;
                                notes.AddRange(resultArray.Select(token =>
                                    JsonConvert.DeserializeObject<Note>(token.ToString())));
                            }
                            

                            page++;
                            retVal.AddRange(notes);
                        }
                        while (notes.Count > 0 & page < 100);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;

                }

                /// <summary>
                /// Gets a comment by numeric ID.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="mergeRequest">The _ merge request.</param>
                /// <param name="id">The _ identifier.</param>
                /// <returns></returns>
                public static Note GetComment(Config config, MergeRequest mergeRequest, int id)
                {
                    string uri = config.APIUrl + "projects/" + mergeRequest.project_id.ToString() + "/merge_requests/" + mergeRequest.id.ToString() + "/notes/" + id.ToString();
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.GET);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                    
                }

                /// <summary>
                /// Updates a comment.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="mergeRequest">The _ merge request.</param>
                /// <param name="note">The _ note.</param>
                /// <returns></returns>
                public static Note UpdateComment(Config config, MergeRequest mergeRequest, Note note)
                {
                    string uri = config.APIUrl + "projects/" + mergeRequest.project_id.ToString() + "/merge_requests/" + mergeRequest.id + "/notes/" + note.id
                        + "?body=" + HttpUtility.UrlEncode(note.body);

                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.PUT);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                    
                }

                /// <summary>
                /// Adds a comment.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="mergeRequest">The _ merge request.</param>
                /// <param name="body">The _ body.</param>
                /// <returns></returns>
                public static Note AddComment(Config config, MergeRequest mergeRequest, string body)
                {
                    string uri = config.APIUrl + "projects/" + mergeRequest.project_id.ToString() + "/merge_requests/" + mergeRequest.id + "/notes?"
                        + "body=" + HttpUtility.UrlEncode(body);
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.POST);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                    
                }
            }

            class MergeRequestList: List<MergeRequest>
            {
                private Project Parent;

                public MergeRequestList(Project _Project)
                {
                    Parent = _Project;
                }

                public void RefreshItems()
                {
                 
                    if (Parent != null)
                    {
                        this.Clear();
                        foreach (MergeRequest M in MergeRequest.List(Parent.Parent.CurrentConfig, Parent))
                        {
                            base.Add(M);
                            M.SetParent(Parent);
                        }
                    }
                    else
                        throw new GitLabStaticAccessException("No parent project available for operation.");
                }

                new public void Add(MergeRequest _MergeRequest)
                {
                    if (Parent != null)
                    {
                        Project P = null;

                        if(_MergeRequest.target_project_id != -1)
                        {
                            P = new Project();
                            P.id = _MergeRequest.target_project_id;
                        }

                        MergeRequest.Create(Parent.Parent.CurrentConfig, Parent, _MergeRequest.source_branch
                            , _MergeRequest.target_branch, _MergeRequest.title, _MergeRequest.description, _MergeRequest.assignee, P, _MergeRequest.labels);

                        RefreshItems();
                    }
                    else
                        throw new GitLabStaticAccessException("No parent project available for operation.");
                }
                

                new public void Remove(MergeRequest _MergeRequest)
                {
                    throw new InvalidOperationException("Removing Merge Requests is not supported by the GitLab API.");
                }
            }
        }
    }
}
