﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Linq;
using RestSharp;

namespace GitLabDotNet
{
    public partial class GitLab
    {
        public partial class Project
        {
            public class Milestone
            {
                public int id, iid, project_id;
                public string title, description, due_date, state, updated_at, created_at;
                public enum StateEvent
                {
                    NONE, ACTIVATE, CLOSE
                }

                /// <summary>
                /// Lists Milestones for the specified Project.
                /// </summary>
                /// <param name="_Config">The _ configuration.</param>
                /// <param name="_Project">The _ project.</param>
                /// <returns></returns>
                /// <exception cref="GitLabServerErrorException"></exception>
                public static List<Milestone> List(Config _Config, Project _Project)
                {
                    List<Milestone> RetVal = new List<Milestone>();

                    try
                    {
                        int page = 1;
                        List<Milestone> milestones = new List<Milestone>();

                        do
                        {
                            milestones.Clear();

                            string URI = _Config.APIUrl + "projects/" + _Project.id.ToString() + "/milestones";

                            URI += "?per_page=100" + "&page=" + page.ToString();

                            RestClient client = new RestClient(URI)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.GET);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", _Config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }
                            dynamic result = JsonConvert.DeserializeObject(response.Content);
                            if (result is JArray)
                            {
                                JArray resultArray = (JArray)result;
                                milestones.AddRange(resultArray.Select(token => JsonConvert.DeserializeObject<Milestone>(token.ToString())));
                            }
                            
                            page++;
                            RetVal.AddRange(milestones);
                        }
                        while (milestones.Count > 0 & page < 100);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return RetVal;
                }

                /// <summary>
                /// Gets the specified Milestone by ID.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="project">The _ project.</param>
                /// <param name="id">The _ identifier.</param>
                /// <returns></returns>
                /// <exception cref="GitLabServerErrorException"></exception>
                public static Milestone Get(Config config, Project project, int id)
                {
                    Milestone retVal;
                    try
                    {
                        string URI = config.APIUrl + "projects/" + project.id.ToString() + "/milestones/" + id.ToString();
                        RestClient client = new RestClient(URI)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        retVal = JsonConvert.DeserializeObject<Milestone>(response.Content);
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                /// <summary>
                /// Creates a new Milestone
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="project">The _ project.</param>
                /// <param name="title">The _ title.</param>
                /// <param name="description">The _ description.</param>
                /// <param name="dueDate">The _ due date.</param>
                /// <returns></returns>
                /// <exception cref="GitLabServerErrorException"></exception>
                // ReSharper disable once UnusedMember.Global
                public static Milestone Create(Config config, Project project, string title, string description = null, DateTime dueDate = new DateTime())
                {
                    Milestone retVal;
                    try
                    {
                        string uri = config.APIUrl + "projects/" + project.id.ToString() + "/milestones/"
                                + "?title=" + HttpUtility.UrlEncode(title);
                        if (description != null)
                            uri += "&description=" + HttpUtility.UrlEncode(description);

                        if (dueDate != new DateTime())
                            uri += "&due_date=" + HttpUtility.UrlEncode(dueDate.ToString("o"));

                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.POST);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        retVal = JsonConvert.DeserializeObject<Milestone>(response.Content);
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                /// <summary>
                /// Updates the Milestone.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="project">The _ project.</param>
                /// <param name="milestone">The _ milestone.</param>
                /// <param name="stateEvent">The _ state event.</param>
                /// <exception cref="GitLabServerErrorException"></exception>
                public static void Update(Config config, Project project, Milestone milestone, StateEvent stateEvent = StateEvent.NONE)
                {
                    try
                    {
                        string uri = config.APIUrl + "projects/" + project.id.ToString() + "/milestones/" + milestone.id.ToString()
                                + "?title=" + HttpUtility.UrlEncode(milestone.title)
                                + "&description=" + HttpUtility.UrlEncode(milestone.description)
                                + "&due_date=" + HttpUtility.UrlEncode(milestone.due_date);

                        if(stateEvent != StateEvent.NONE)
                            uri += "&state_event=" + HttpUtility.UrlEncode(stateEvent.ToString().ToLowerInvariant());
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.PUT);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }


                /// <summary>
                /// Lists the comments associated with the Milestone
                /// </summary>
                /// <param name="_Config">The _ configuration.</param>
                /// <param name="_Project">The _ project.</param>
                /// <param name="_Snippet">The _ snippet.</param>
                /// <returns></returns>
                public static List<Note> ListComments(Config _Config, Project _Project, Milestone _Milestone)
                {
                    List<Note> RetVal = new List<Note>();

                    try
                    {
                        int page = 1;
                        List<Note> notes = new List<Note>();

                        do
                        {
                            notes.Clear();

                            string uri = _Config.APIUrl;

                            uri += "projects/" + _Milestone.project_id.ToString() + "/milestones/" + _Milestone.id.ToString() + "/notes";


                            uri += "?per_page=100"
                                    + "&page=" + page.ToString();

                            RestClient client = new RestClient(uri)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.GET);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", _Config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }
                            dynamic result = JsonConvert.DeserializeObject(response.Content);
                            if (result is JArray)
                            {
                                JArray resultArray = (JArray)result;
                                notes.AddRange(resultArray.Select(token =>
                                    JsonConvert.DeserializeObject<Note>(token.ToString())));
                            }
                            

                            page++;
                            RetVal.AddRange(notes);
                        }
                        while (notes.Count > 0 & page < 100);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return RetVal;

                }

                /// <summary>
                /// Gets a comment by ID.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="_Project">The _ project.</param>
                /// <param name="milestone">The milestone.</param>
                /// <param name="id">The _ identifier.</param>
                /// <returns></returns>
                // ReSharper disable once UnusedMember.Global
                public static Note GetComment(Config config, Milestone milestone, int id)
                {
                    string uri = config.APIUrl + "projects/" + milestone.project_id.ToString() + "/milestones/" +
                                 milestone.id.ToString() + "/notes/" + id.ToString();
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.GET);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                    
                }

                /// <summary>
                /// Updates a comment.
                /// </summary>
                /// <param name="config">The configuration.</param>
                /// <param name="milestone">The milestone.</param>
                /// <param name="note">The note.</param>
                /// <returns>A GitLab Note object.</returns>
                // ReSharper disable once UnusedMember.Global
                public static Note UpdateComment(Config config, Milestone milestone, Note note)
                {
                    string uri = config.APIUrl + "projects/" + milestone.project_id.ToString() + "/milestones/" +
                                 milestone.id + "/notes/" + note.id
                                 + "?body=" + HttpUtility.UrlEncode(note.body);
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.PUT);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                }

                /// <summary>
                /// Adds a comment to the Milestone.
                /// </summary>
                /// <param name="config">The configuration.</param>
                /// <param name="milestone">The milestone.</param>
                /// <param name="body">The body.</param>
                /// <returns>A GitLab Note object.</returns>
                public static Note AddComment(Config config, Milestone milestone, string body)
                {
                    string uri = config.APIUrl + "projects/" + milestone.project_id.ToString() + "/milestones/" +
                                 milestone.id.ToString() + "/notes?"
                                 + "body=" + HttpUtility.UrlEncode(body);
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.POST);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                    
                }

            }
        }
    }
}
