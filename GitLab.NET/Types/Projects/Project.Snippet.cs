﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Linq;
using RestSharp;

namespace GitLabDotNet
{
    public partial class GitLab
    {
        public partial class Project
        {
            public class Snippet
            {
                public int id;
                public string title, file_name, expires_at, updated_at, created_at;
                public User author;

                /// <summary>
                ///  Get a list of project snippets.
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <returns>A List of GitLab Snippets.</returns>
                public static List<Snippet> List(Config config, Project project)
                {
                    List<Snippet> retVal = new List<Snippet>();

                    try
                    {
                        int page = 1;
                        List<Snippet> snippets = new List<Snippet>();

                        do
                        {
                            snippets.Clear();

                            string uri = config.APIUrl +  "projects/" + project.id.ToString() + "/snippets" ; 
                            
                            uri += "?per_page=100" + "&page=" + page.ToString();

                            RestClient client = new RestClient(uri)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.GET);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }
                            dynamic result = JsonConvert.DeserializeObject(response.Content);
                            if (result is JArray)
                            {
                                JArray resultArray = (JArray)result;
                                snippets.AddRange(resultArray.Select(token =>
                                    JsonConvert.DeserializeObject<Snippet>(token.ToString())));
                            }
                            
                            page++;
                            retVal.AddRange(snippets);
                        }
                        while (snippets.Count > 0 & page < 100);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                /// <summary>
                /// Get a single project snippet
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <param name="id"></param>
                /// <returns>A GitLab Snippet</returns>
                public static Snippet Get(Config config, Project project, int id)
                {
                    Snippet retVal;
                    try
                    {
                        string uri = config.APIUrl + "projects/" + project.id.ToString() + "/snippets/" + id.ToString();
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        retVal = JsonConvert.DeserializeObject<Snippet>(response.Content);
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                /// <summary>
                /// Gets the raw content of a snippet.
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project">Project Descriptor object that snippet belongs to</param>
                /// <param name="id">Snippet ID</param>
                /// <returns>The Raw Stream</returns>
                public static Stream GetContent(Config config, Project project, int id)
                {
                    Stream retVal = null;
                    try
                    {
                        string URI = config.APIUrl + "projects/" + project.id.ToString() + "/snippets/" + id.ToString() + "/raw";
                        RestClient client = new RestClient(URI)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }

                        //retVal = response.Content;

                        //HttpResponse<Stream> R = Unirest.get(URI)                                    
                        //            .header("PRIVATE-TOKEN", config.APIKey)
                        //            .asBinary();

                        //if (R.Code < 200 || R.Code >= 300)
                        //{
                        //    throw new GitLabServerErrorException(new StreamReader(R.Body).ReadToEnd(), R.Code);
                        //}
                        //else
                        //{
                        //    retVal = R.Body;
                        //}
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                public static Snippet Create(Config config, Project project, string title, string fileName, string code,
                    VisibilityLevel visibilityLevel)
                {
                    Snippet retVal;
                    try
                    {
                        string uri = config.APIUrl + "projects/" + project.id.ToString() + "/snippets/"
                                + "?title=" + HttpUtility.UrlEncode(title)
                                + "&file_name=" + HttpUtility.UrlEncode(fileName)
                                + "&code=" + HttpUtility.UrlEncode(code)
                                + "&visibility_level=" + Convert.ToInt64(visibilityLevel).ToString();
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.POST);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        retVal = JsonConvert.DeserializeObject<Snippet>(response.Content);
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                /// <summary>
                /// Delete snippet from a project.
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <param name="snippet"></param>
                /// <returns></returns>
                public static Snippet Delete(Config config, Project project, Snippet snippet)
                {
                    Snippet retVal;

                    try
                    {
                        string uri = config.APIUrl + "projects/" + project.id.ToString() + "/snippets/" + snippet.id.ToString();
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.DELETE);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        retVal = JsonConvert.DeserializeObject<Snippet>(response.Content);
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    return retVal;
                }

                /// <summary>
                /// Update the properties of a project snippet
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <param name="snippet"></param>
                /// <param name="newCode"></param>
                /// <param name="newVisibilityLevel"></param>
                // ReSharper disable once UnusedMember.Global
                public static void Update(Config config, Project project, Snippet snippet, string newCode = null,
                    VisibilityLevel newVisibilityLevel = VisibilityLevel.Undefined)
                {                   
                    try
                    {
                        string uri = config.APIUrl + "projects/" + project.id.ToString() + "/snippets/" + snippet.id.ToString()
                                + "?title=" + HttpUtility.UrlEncode(snippet.title)
                                + "&file_name=" + HttpUtility.UrlEncode(snippet.file_name);

                        if (newCode != null)
                            uri += "&code=" + HttpUtility.UrlEncode(newCode);

                        if(newVisibilityLevel != VisibilityLevel.Undefined)
                            uri += "&visibility_level=" + Convert.ToInt64(newVisibilityLevel).ToString();
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.PUT);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }                
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                /// <summary>
                /// Lists the comments associated with the Snippet
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="project">The _ project.</param>
                /// <param name="snippet">The _ snippet.</param>
                /// <returns></returns>
                // ReSharper disable once UnusedMember.Global
                public static List<Note> ListComments(Config config, Project project, Snippet snippet)
                {
                    List<Note> retVal = new List<Note>();

                    try
                    {
                        int page = 1;
                        List<Note> notes = new List<Note>();

                        do
                        {
                            notes.Clear();

                            string uri = config.APIUrl;

                            uri += "projects/" + project.ToString() + "/snippets/" + snippet.id.ToString() + "/notes";


                            uri += "?per_page=100"
                                    + "&page=" + page.ToString();
                            RestClient client = new RestClient(uri)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.GET);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }
                            
                            dynamic result = JsonConvert.DeserializeObject(response.Content);
                            if (result is JArray)
                            {
                                JArray resultArray = (JArray)result;
                                notes.AddRange(resultArray.Select(token =>
                                    JsonConvert.DeserializeObject<Note>(token.ToString())));
                            }
                            page++;
                            retVal.AddRange(notes);
                        }
                        while (notes.Count > 0 & page < 100);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;

                }

                /// <summary>
                /// Gets a specific comment from the Snippet by Note ID
                /// </summary>
                /// <param name="config">The configuration.</param>
                /// <param name="project">The project.</param>
                /// <param name="snippet">The snippet.</param>
                /// <param name="id">The identifier.</param>
                /// <returns></returns>
                // ReSharper disable once UnusedMember.Global
                public static Note GetComment(Config config, Project project, Snippet snippet, int id)
                {
                    string uri = config.APIUrl + "projects/" + project.id.ToString() + "/snippets/" + snippet.id.ToString() + "/notes/" + id.ToString();
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.GET);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                    
                }

                /// <summary>
                /// Updates a comment.
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="project">The _ project.</param>
                /// <param name="snippet">The _ snippet.</param>
                /// <param name="note">The _ note.</param>
                /// <returns></returns>
                // ReSharper disable once UnusedMember.Global
                public static Note UpdateComment(Config config, Project project, Snippet snippet, Note note)
                {
                    string uri = config.APIUrl + "projects/" + project.id.ToString() + "/snippets/" + snippet.id + "/notes/" + note.id
                        + "?body=" + HttpUtility.UrlEncode(note.body);
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.PUT);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                    
                }

                /// <summary>
                /// Adds a comment to the Snippet
                /// </summary>
                /// <param name="config">The _ configuration.</param>
                /// <param name="project">The _ project.</param>
                /// <param name="snippet">The _ snippet.</param>
                /// <param name="body">The _ body.</param>
                /// <returns></returns>
                // ReSharper disable once UnusedMember.Global
                public static Note AddComment(Config config, Project project, Snippet snippet, string body)
                {
                    string uri = config.APIUrl + "projects/" + project.id.ToString() + "/snippets/" + snippet.id.ToString() + "/notes?"
                        + "body=" + HttpUtility.UrlEncode(body);
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.POST);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<Note>(response.Content);
                }
            }
        }
    }
}
