﻿using System;
using Newtonsoft.Json;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GitLabDotNet
{
    partial class GitLab
    {
        partial class Project
        {
            /// <summary>
            /// Creates a new project
            /// </summary>
            /// <param name="config"></param>
            /// <param name="name">The name of the new project</param>
            /// <param name="description"> Optional Description, can be null</param>
            /// <param name="namespace">Namespace to create the project in</param>
            /// <param name="visibilityLevel">New project's visibility level</param>
            /// <returns></returns>
            public static Project Create(Config config, string name, string description, Namespace @namespace,
                VisibilityLevel visibilityLevel)
            {
                string uri = config.APIUrl + "projects?name=" + HttpUtility.UrlEncode(name)
                             + "&namespace_id=" + @namespace.id.ToString()
                             + "&description=" + HttpUtility.UrlEncode(description)
                             + "&visibility_level=" + Convert.ToInt64(visibilityLevel).ToString();
                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }

                return JsonConvert.DeserializeObject<Project>(response.Content);
            }

            /// <summary>
            /// Update any project
            /// </summary>
            /// <param name="config">Gitlab Config Object</param>
            /// <param name="project">Project object</param>
            /// <returns></returns>
            // ReSharper disable once UnusedMember.Global
            public static Project Update(Config config, Project project)
            {

                string uri = config.APIUrl + "projects/" + project.id.ToString()
                                        + "?name=" + HttpUtility.UrlEncode(project.name)
                                        + "&path=" + HttpUtility.UrlEncode(project.path)
                                        + "&description=" + HttpUtility.UrlEncode(project.description);

                if (!string.IsNullOrWhiteSpace(project.default_branch))
                    uri +=                "&default_branch=" + HttpUtility.UrlEncode(project.default_branch);

                uri +=                    "&issues_enabled=" + Convert.ToInt32(project.issues_enabled).ToString()
                                        + "&merge_requests_enabled=" + Convert.ToInt32(project.merge_requests_enabled).ToString()
                                        + "&builds_enabled=" + Convert.ToInt32(project.builds_enabled).ToString()
                                        + "&wiki_enabled=" + Convert.ToInt32(project.wiki_enabled).ToString()
                                        + "&snippets_enabled=" + Convert.ToInt32(project.snippets_enabled).ToString()
                                        + "&visibility_level=" + Convert.ToInt64(project.visibility_level).ToString();

                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.PUT);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
                return JsonConvert.DeserializeObject<Project>(response.Content);
            }

            /// <summary>
            /// Deletes a project
            /// </summary>
            /// <param name="config"></param>
            /// <param name="project"></param>
            public static void Delete(Config config, Project project)
            {
                string uri = config.APIUrl + "projects/" + project.id.ToString();
                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.DELETE);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
            }

            /// <summary>
            /// List all projects
            /// </summary>
            /// <param name="config"></param>
            /// <returns></returns>
            public static List<Project> List(Config config)
            {
                List<Project> RetVal = new List<Project>();

                try
                {
                    int page = 1;
                    List<Project> projects = new List<Project>();

                    do
                    {
                        projects.Clear();

                        string uri = config.APIUrl + "projects?per_page=100" + "&page=" + page.ToString();
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        
                        dynamic result = JsonConvert.DeserializeObject(response.Content);
                        if (result is JArray)
                        {
                            JArray resultArray = (JArray)result;
                            projects.AddRange(resultArray.Select(token =>
                                JsonConvert.DeserializeObject<Project>(token.ToString())));
                        }
                        page++;
                        RetVal.AddRange(projects);
                    }
                    while (projects.Count > 0 & page < 100);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RetVal;
            }

            /// <summary>
            /// Get single project by numeric ID
            /// </summary>
            /// <param name="config"></param>
            /// <param name="id"></param>
            /// <returns></returns>
            public static Project Get(Config config, int id)
            {
                Project retVal = null;

                try
                {
                    string uri = config.APIUrl + "projects/" + id.ToString();
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.GET);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    retVal = JsonConvert.DeserializeObject<Project>(response.Content);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return retVal;
            }


            /// <summary>
            /// Query projects by name
            /// </summary>
            /// <param name="config"></param>
            /// <param name="query">The value to search.</param>
            /// <returns></returns>
            public static List<Project> Search(Config config, string query)
            {
                List<Project> RetVal = new List<Project>();

                try
                {
                    int page = 1;
                    List<Project> projects = new List<Project>();

                    do
                    {
                        projects.Clear();
                        string uri = config.APIUrl + "projects/search/" + HttpUtility.UrlEncode(query)
                                     + "?per_page=100"
                                     + "&page=" + page.ToString();
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        dynamic result = JsonConvert.DeserializeObject(response.Content);
                        if (result is JArray)
                        {
                            JArray resultArray = (JArray)result;
                            projects.AddRange(resultArray.Select(token =>
                                JsonConvert.DeserializeObject<Project>(token.ToString())));
                        }
                        
                        page++;
                        RetVal.AddRange(projects);
                    }
                    while (projects.Count > 0 & page < 100);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RetVal;
            }

            /// <summary>
            /// Get a list of project members viewable by the authenticated user.
            /// </summary>
            /// <param name="config"></param>
            /// <param name="project"></param>
            /// <returns></returns>
            public static List<Member> ListMembers(Config config, Project project)
            {
                List<Member> retVal = new List<Member>();

                try
                {
                    int page = 1;
                    List<Member> members = new List<Member>();

                    do
                    {
                        members.Clear();

                        string uri = (config.APIUrl + "projects/" + project.id.ToString() + "/members"
                                + "?per_page=100"
                                + "&page=" + page.ToString());

                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        dynamic result = JsonConvert.DeserializeObject(response.Content);
                        if (result is JArray)
                        {
                            JArray resultArray = (JArray)result;
                            members.AddRange(resultArray.Select(token =>
                                JsonConvert.DeserializeObject<Member>(token.ToString())));
                        }
                        
                        page++;
                        retVal.AddRange(members);
                    }
                    while (members.Count > 0 & page < 100);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return retVal;
            }

            /// <summary>
            /// Adds a user to the list of project members.
            /// </summary>
            /// <param name="config"></param>
            /// <param name="project"></param>
            /// <param name="user"></param>
            /// <param name="accessLevel"></param>
            public static void AddMember(Config config, Project project, User user, Member.AccessLevel accessLevel)
            {
                //OWNER access level only valid for groups according to https://docs.gitlab.com/ce/api/access_requests.html
                if (accessLevel > Member.AccessLevel.MASTER)
                    accessLevel = Member.AccessLevel.MASTER;

                string uri = config.APIUrl + "projects/" + project.id.ToString() + "/members/?user_id=" + user.id + "&access_level=" + Convert.ToInt64(accessLevel);
                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
            }

            /// <summary>
            /// Updates a project team member to a specified access level.
            /// </summary>
            /// <param name="config"></param>
            /// <param name="project"></param>
            /// <param name="user"></param>
            /// <param name="accessLevel"></param>
            public static void UpdateMember(Config config, Project project, User user, Member.AccessLevel accessLevel)
            {
                //OWNER access level only valid for groups according to https://docs.gitlab.com/ce/api/access_requests.html
                if (accessLevel > Member.AccessLevel.MASTER)
                    accessLevel = Member.AccessLevel.MASTER;

                string uri = config.APIUrl + "projects/" + project.id.ToString() + "/members/" + user.id + "&access_level=" + Convert.ToInt64(accessLevel);
                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.PUT);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
            }

            /// <summary>
            /// Removes a user from a project team.
            /// </summary>
            /// <param name="config"></param>
            /// <param name="project"></param>
            /// <param name="user"></param>
            public static void DeleteMember(Config config, Project project, User user)
            {
                string uri = config.APIUrl + "projects/" + project.id.ToString() + "/members/" + user.id;
                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.DELETE);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
            }
        }
    }
}