﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json;
using RestSharp;

namespace GitLabDotNet
{
    partial class GitLab
    {
        partial class Project
        {
            public partial class Upload
            {
                private string atl = "";
                private string url = "";
                private string full_path = "";
                private string markdown ="";

                public string Atl { get; set; }
                public string Url { get; set; }
                public string FullPath { get; set; }
                public string Markdown { get; set; }
                
                public static List<Upload> Add(Config _Config, Project _Project, ObservableCollection<string> _upload)
                {
                    List<Upload> uploads = new List<Upload>();
                    for (int i = 0; i < _upload.Count; i++)
                    {
                        RestClient client = new RestClient(_Config.APIUrl + "projects/" + _Project.id.ToString() + "/uploads")
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.POST);
                        request.AddHeader("Authorization", string.Concat("Bearer ", _Config.APIKey));
                        request.AddHeader("Content-Type", "multipart/form-data");
                        string k = _upload[i].Normalize();
                        //string r = _upload[i].Replace(@"\", @"/");
                        //string s = r.Insert(0, @"/");
                        request.AddFile("file", k);
                        IRestResponse response = client.Execute(request);
                        HttpStatusCode statusCode = response.StatusCode;
                        Console.WriteLine(response.Content);
                        int responseCode = Convert.ToInt32(statusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        Console.WriteLine(response.Content);
                        uploads.Add(JsonConvert.DeserializeObject<Upload>(response.Content));
                    }

                    return uploads;
                }
            }
        }
    }
}
