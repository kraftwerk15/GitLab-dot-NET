﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GitLabDotNet
{
    partial class GitLab
    {
        partial class Project
        {
            /// <summary>
            /// The webhooks that belong to this project.
            /// </summary>
            private WebhookList _Webhooks;
                
            public WebhookList Webhooks
            {
                get
                {
                    if (_Webhooks == null)
                        _Webhooks = new WebhookList(this);
                    return _Webhooks;                    
                }
            }                      
         
            public partial class Webhook
            {
                public int id, project_id;
                public string url, created_at;
                public bool push_events, tag_push_events, issues_events, merge_requests_events, note_events, enable_ssl_verification;

                public Project Parent
                {
                    get
                    {
                        return _Parent;
                    }
                }

                internal Project _Parent;

                /// <summary>
                /// Updates this webhook on the server.
                /// </summary>
                public void Update()
                {
                    if (Parent != null)
                    {
                        Webhook.Update(Parent.Parent.CurrentConfig, this, Parent);
                    }
                    else
                    {
                        throw new GitLabStaticAccessException("Unable to save Webhook without parent project");
                    }
                }

                /// <summary>
                /// List all webhooks for a project
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <returns></returns>
                public static List<Webhook> List(Config config, Project project)
                {
                    List<Webhook> retVal = new List<Webhook>();

                    try
                    {
                        int page = 1;
                        List<Webhook> webhooks = new List<Webhook>();

                        do
                        {
                            webhooks.Clear();

                            string uri = config.APIUrl + "projects/" + project.id + "/hooks";
                            uri += "?per_page=100" + "&page=" + page.ToString();
                            RestClient client = new RestClient(uri)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.GET);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }
                            
                            dynamic result = JsonConvert.DeserializeObject(response.Content);
                            if (result is JArray)
                            {
                                JArray resultArray = (JArray)result;
                                webhooks.AddRange(resultArray.Select(token =>
                                    JsonConvert.DeserializeObject<Webhook>(token.ToString())));
                            }
                            
                            page++;
                            retVal.AddRange(webhooks);
                        }
                        while (webhooks.Count > 0 & page < 100);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                /// <summary>
                /// Get Project Webhook by numerif ID
                /// </summary>
                /// <param name="config"></param>
                /// <param name="project"></param>
                /// <param name="id"></param>
                /// <returns></returns>
                public static Webhook Get(Config config, Project project, int id)
                {
                    try
                    {
                        string uri = config.APIUrl + "/projects/" + project.id + "/hooks/" + id.ToString();
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        
                        return JsonConvert.DeserializeObject<Webhook>(response.Content);
                        
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                /// <summary>
                /// Adds a new webhook to a project
                /// </summary>
                /// <param name="config"></param>
                /// <param name="webhook"></param>
                /// <param name="project"></param>
                public static void Add(Config config, Webhook webhook, Project project)
                {
                    try
                    {
                        string uri = config.APIUrl + "/projects/"+project.id.ToString()+"/hooks";

                        uri += "?url=" + HttpUtility.UrlEncode(webhook.url)
                            + "&push_events=" + Convert.ToInt32(webhook.push_events).ToString()
                            + "&issues_events=" + Convert.ToInt32(webhook.issues_events).ToString()
                            + "&merge_requests_events=" + Convert.ToInt32(webhook.merge_requests_events).ToString()
                            + "&tag_push_events=" + Convert.ToInt32(webhook.tag_push_events).ToString()
                            + "&note_events=" + Convert.ToInt32(webhook.note_events).ToString()
                            + "&enable_ssl_verification=" + Convert.ToInt32(webhook.enable_ssl_verification).ToString();
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.POST);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }


                /// <summary>
                /// Updates a project webhook on the GitLab server.
                /// </summary>
                /// <param name="config"></param>
                /// <param name="webhook"></param>
                /// <param name="project"></param>
                public static void Update(Config config, Webhook webhook, Project project)
                {
                    try
                    {
                        string uri = config.APIUrl + "/projects/" + project.id.ToString() + "/hooks/" + webhook.id;

                        uri += "?url=" + HttpUtility.UrlEncode(webhook.url)
                            + "&push_events=" + Convert.ToInt32(webhook.push_events).ToString()
                            + "&issues_events=" + Convert.ToInt32(webhook.issues_events).ToString()
                            + "&merge_requests_events=" + Convert.ToInt32(webhook.merge_requests_events).ToString()
                            + "&tag_push_events=" + Convert.ToInt32(webhook.tag_push_events).ToString()
                            + "&note_events=" + Convert.ToInt32(webhook.note_events).ToString()
                            + "&enable_ssl_verification=" + Convert.ToInt32(webhook.enable_ssl_verification).ToString();
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.PUT);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                /// <summary>
                /// Deletes a project webhook from the server. 
                /// </summary>
                /// <param name="config"></param>
                /// <param name="webhook"></param>
                /// <param name="project"></param>
                public static void Delete(Config config, Webhook webhook, Project project)
                {
                    try
                    {
                        string uri = config.APIUrl + "/projects/" + project.id.ToString() + "/hooks" + webhook.id;
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.DELETE);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }

            public class WebhookList : List<Webhook>
            {
                Project Parent;

                public WebhookList(Project _Parent)
                {
                    Parent = _Parent;
                    RefreshItems();
                }

                public void RefreshItems()
                {
                    if (Parent != null)
                    {
                        this.Clear();

                        foreach (Webhook W in Webhook.List(Parent.Parent.CurrentConfig, Parent))
                        {
                            base.Add(W);
                        }
                    }
                    else
                        throw new GitLabStaticAccessException("No Parent Project for operation");
                }

                new public void Add(Webhook _Webhook)
                {
                    if (Parent != null)
                    {
                        Webhook.Add(Parent.Parent.CurrentConfig, _Webhook, Parent);
                        RefreshItems();
                    }
                    else
                        throw new GitLabStaticAccessException("No parent project available for operation.");
                }

                new public void Remove(Webhook _Webhook)
                {
                    if (Parent != null)
                    {
                        Webhook.Delete(Parent.Parent.CurrentConfig, _Webhook, Parent);
                        RefreshItems();
                    }
                    else
                        throw new GitLabStaticAccessException("No parent project available for operation.");
                }
            }
        }
    }
}
