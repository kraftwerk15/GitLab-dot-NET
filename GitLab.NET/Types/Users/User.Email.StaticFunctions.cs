﻿using System;
using Newtonsoft.Json;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GitLabDotNet
{
    partial class GitLab
    {
        partial class User
        {
            partial class Email
            {

                public static List<Email> List(Config config, User user = null)
                {
                    List<Email> retVal = new List<Email>();

                    try
                    {
                        int page = 1;
                        List<Email> emails = new List<Email>();

                        do
                        {
                            emails.Clear();

                            string uri = config.APIUrl;

                            if (user == null)
                                uri += "user/emails/" + user.id.ToString();
                            else
                                uri += "users/" + user.id.ToString() + "/emails/" + user.id.ToString();

                            uri += "?per_page=100"
                                    + "&page=" + page.ToString();

                            RestClient client = new RestClient(uri)
                            {
                                Timeout = -1
                            };
                            RestRequest request = new RestRequest(Method.GET);
                            request.AddHeader("Accept", "application/json");
                            request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                            IRestResponse response = client.Execute(request);
                            int responseCode = Convert.ToInt32(response.StatusCode);
                            if (responseCode < 200 || responseCode >= 300)
                            {
                                throw new GitLabServerErrorException(response.Content, responseCode);
                            }

                            dynamic result = JsonConvert.DeserializeObject(response.Content);

                            if (result is JArray)
                            {
                                JArray resultArray = (JArray)result;
                                emails.AddRange(resultArray.Select(token =>
                                    JsonConvert.DeserializeObject<Email>(token.ToString())));
                            }
                            
                            page++;
                            retVal.AddRange(emails);
                        }
                        while (emails.Count > 0 & page < 100);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    return retVal;
                }

                /// <summary>
                /// Adds an email address to a user account. If _User is null adds to current user.
                /// </summary>
                /// <param name="config"></param>
                /// <param name="email"></param>
                /// <param name="user"></param>
                public static void Add(Config config, string email, User user = null)
                {
                    try
                    {
                        string uri = config.APIUrl;

                        if (user == null)
                            uri += "user/emails/";
                        else
                            uri += "users/" + user.id.ToString() + "/emails/";

                        uri += "?email=" + HttpUtility.UrlEncode(email);

                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.POST);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }

                /// <summary>
                /// Deletes an email address from a user account. If _User is null, deletes for current user.
                /// </summary>
                /// <param name="config"></param>
                /// <param name="email"></param>
                /// <param name="user"></param>
                public static void Delete(Config config, Email email, User user = null)
                {
                    try
                    {
                        string uri = config.APIUrl;

                        if (user == null)
                            uri += "user/emails/" + email.id.ToString();
                        else
                            uri += "users/" + user.id.ToString() + "/emails/" + email.id.ToString();
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.DELETE);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }
    }
}
