﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Web;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GitLabDotNet
{
    partial class GitLab
    {
        partial class SSHKey
        {

            /// <summary>
            /// Gets list of SSH Keys for a user. Gets keys for the current user if _User is null.
            /// </summary>
            /// <param name="config"></param>
            /// <param name="user"></param>
            /// <returns></returns>
            public static List<SSHKey> List(Config config, User user = null)
            {
                List<SSHKey> retVal = new List<SSHKey>();

                try
                {
                    int page = 1;
                    List<SSHKey> keys = new List<SSHKey>();

                    do
                    {
                        keys.Clear();

                        string uri = config.APIUrl;

                        if (user == null)
                            uri += "user/keys";
                        else
                            uri += "users/" + user.id.ToString() + "/keys";

                        uri += "?per_page=100" + "&page=" + page.ToString();

                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                        
                        dynamic result = JsonConvert.DeserializeObject(response.Content);
                        if (result is JArray)
                        {
                            JArray resultArray = (JArray)result;
                            keys.AddRange(resultArray.Select(token =>
                                JsonConvert.DeserializeObject<SSHKey>(token.ToString())));
                        }
                        
                        page++;
                        retVal.AddRange(keys);
                    }
                    while (keys.Count > 0 & page < 100);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return retVal;
            }

            /// <summary>
            /// Get SSHKey by numeric ID.
            /// </summary>
            /// <param name="config"></param>
            /// <param name="user"></param>
            /// <param name="id"></param>
            /// <returns></returns>
            public static SSHKey Get(Config config, User user, int id)
            {
                try
                {
                    string uri = config.APIUrl;

                    if (user == null)
                        uri += "user/keys/";
                    else
                        uri += "users/" + user.id.ToString() + "/keys/";

                    uri +=  id.ToString();

                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.GET);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<SSHKey>(response.Content);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// Adds SSH key to user account. If _User is null adds for current user. 
            /// </summary>            
            /// <param name="config"></param>
            /// <param name="sshKey"></param>
            /// <param name="user"></param>
            public static void Add(Config config, SSHKey sshKey, User user = null)
            {
                try
                {
                    string uri = config.APIUrl;

                    if (user == null)
                        uri += "user/keys";
                    else
                        uri += "users/" + user.id.ToString() + "/keys";

                    uri += "?title=" + HttpUtility.UrlEncode(sshKey.title)
                     + "&key=" + HttpUtility.UrlEncode(sshKey.key);

                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.POST);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// Deletes SSH key to user account. If _User is null adds for current user. 
            /// </summary>
            /// <param name="config"></param>
            /// <param name="sshKey"></param>
            /// <param name="user"></param>
            public static void Delete(Config config, SSHKey sshKey, User user = null)
            {
                try
                {
                    string uri = config.APIUrl;

                    if (user == null)
                        uri += "user/keys/" + sshKey.id.ToString();
                    else
                        uri += "users/" + user.id.ToString() + "/keys/" + sshKey.id.ToString();
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.DELETE);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}
