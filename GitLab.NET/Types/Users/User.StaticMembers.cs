﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Web;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace GitLabDotNet
{
    public partial class GitLab
    {
        public partial class User
        {
            /// <summary>
            /// List all users that the current user can see 
            /// </summary>
            /// <param name="config">A GitLab.NET Configuration object</param>
            /// <returns></returns>
            public static List<User> List(Config config, GitLab parent = null)
            {
                List<User> RetVal = new List<User>();

                try
                {
                    int page = 1;
                    List<User> users = new List<User>();

                    do
                    {
                        users.Clear();

                        string uri = config.APIUrl + "users?per_page=100" + "&page=" + page.ToString();
                        RestClient client = new RestClient(uri)
                        {
                            Timeout = -1
                        };
                        RestRequest request = new RestRequest(Method.GET);
                        request.AddHeader("Accept", "application/json");
                        request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                        IRestResponse response = client.Execute(request);
                        int responseCode = Convert.ToInt32(response.StatusCode);
                        if (responseCode < 200 || responseCode >= 300)
                        {
                            throw new GitLabServerErrorException(response.Content, responseCode);
                        }
                    
                        dynamic result = JsonConvert.DeserializeObject(response.Content);
                        if (result is JArray)
                        {
                            JArray resultArray = (JArray)result;
                            foreach (JToken token in resultArray)
                            {
                                User u = JsonConvert.DeserializeObject<User>(token.ToString());
                                u.SetParent(parent);                               
                                users.Add(u);
                            }
                        }
                        
                        page++;
                        RetVal.AddRange(users);
                    }
                    while (users.Count > 0 & page < 100);
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                return RetVal;

            }

            /// <summary>
            /// Get user object by numeric ID
            /// </summary>
            /// <param name="config">A GitLab.NET Configuration object</param>
            /// <param name="id">Numeric ID of the user to retrieve. Returns current user if less than 0.</param>
            /// <returns></returns>
            public static User Get(Config config, int id = -1)
            {
                try
                {
                    string uri = config.APIUrl;

                    if (id > 0)
                        uri += "users/" + id.ToString();
                    else
                        uri += "user/";

                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.GET);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                    return JsonConvert.DeserializeObject<User>(response.Content);
                    
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            /// <summary>
            /// Create a new user on the GitLab server
            /// </summary>
            /// <param name="config">A GitLab.NET Configuration object</param>
            /// <param name="user">User object to create on the server</param>
            public static User Create(Config config, User user, string password, bool requireConfirmation = true)
            {
                string uri = config.APIUrl + "users?"
                     + "email=" + HttpUtility.UrlEncode(user.email)
                     + "&password=" + HttpUtility.UrlEncode(password)
                     + "&username=" + HttpUtility.UrlEncode(user.username)
                     + "&name=" + HttpUtility.UrlEncode(user.name)
                     + "&confirm=" + Convert.ToInt64(requireConfirmation).ToString();

                if (!string.IsNullOrWhiteSpace(user.skype))
                    uri += "&skype=" + HttpUtility.UrlEncode(user.skype);
                if (!string.IsNullOrWhiteSpace(user.linkedin))
                    uri += "&linkedin=" + HttpUtility.UrlEncode(user.linkedin);
                if (!string.IsNullOrWhiteSpace(user.twitter))
                    uri += "&twitter=" + HttpUtility.UrlEncode(user.twitter);
                if (!string.IsNullOrWhiteSpace(user.web_url))
                    uri += "&web_url=" + HttpUtility.UrlEncode(user.web_url);
                if (!string.IsNullOrWhiteSpace(user.website_url))
                    uri += "&website_url=" + HttpUtility.UrlEncode(user.website_url);
                if (user.projects_limit >= 0)
                    uri += "&projects_limit=" + user.projects_limit.ToString();
                if (!string.IsNullOrWhiteSpace(user.extern_uid))
                    uri += "&extern_uid=" + HttpUtility.UrlEncode(user.extern_uid);
                if (!string.IsNullOrWhiteSpace(user.provider))
                    uri += "&provider=" + HttpUtility.UrlEncode(user.provider);
                if (!string.IsNullOrWhiteSpace(user.bio))
                    uri += "&bio=" + HttpUtility.UrlEncode(user.bio);

                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.POST);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
                
                return JsonConvert.DeserializeObject<User>(response.Content);
            }

            /// <summary>
            /// Modifies an existing user. Only administrators can change attributes of a user.
            /// </summary>
            /// <param name="config"></param>
            /// <param name="user"></param>
            /// <param name="password"></param>
            public static void Update(Config config, User user, string password = null)
            {

                string uri = config.APIUrl + "users/"+user.id.ToString()
                     + "?email=" + HttpUtility.UrlEncode(user.email)         
                     + "&username=" + HttpUtility.UrlEncode(user.username)
                     + "&name=" + HttpUtility.UrlEncode(user.name);

                if (!string.IsNullOrWhiteSpace(password))
                    uri += "&password=" + HttpUtility.UrlEncode(password);
                if (!string.IsNullOrWhiteSpace(user.skype))
                    uri += "&skype=" + HttpUtility.UrlEncode(user.skype);
                if (!string.IsNullOrWhiteSpace(user.linkedin))
                    uri += "&linkedin=" + HttpUtility.UrlEncode(user.linkedin);
                if (!string.IsNullOrWhiteSpace(user.twitter))
                    uri += "&twitter=" + HttpUtility.UrlEncode(user.twitter);
                if (!string.IsNullOrWhiteSpace(user.web_url))
                    uri += "&web_url=" + HttpUtility.UrlEncode(user.web_url);
                if (!string.IsNullOrWhiteSpace(user.website_url))
                    uri += "&website_url=" + HttpUtility.UrlEncode(user.website_url);
                if (user.projects_limit >= 0)
                    uri += "&projects_limit=" + user.projects_limit.ToString();
                if (!string.IsNullOrWhiteSpace(user.extern_uid))
                    uri += "&extern_uid=" + HttpUtility.UrlEncode(user.extern_uid);
                if (!string.IsNullOrWhiteSpace(user.provider))
                    uri += "&provider=" + HttpUtility.UrlEncode(user.provider);
                if (!string.IsNullOrWhiteSpace(user.bio))
                    uri += "&bio=" + HttpUtility.UrlEncode(user.bio);
                
                uri += "&admin=" + Convert.ToInt32(user.is_admin).ToString();
                uri += "&can_create_group=" + Convert.ToInt32(user.can_create_group).ToString();

                RestClient client = new RestClient(uri)
                {
                    Timeout = -1
                };
                RestRequest request = new RestRequest(Method.PUT);
                request.AddHeader("Accept", "application/json");
                request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                IRestResponse response = client.Execute(request);
                int responseCode = Convert.ToInt32(response.StatusCode);
                if (responseCode < 200 || responseCode >= 300)
                {
                    throw new GitLabServerErrorException(response.Content, responseCode);
                }
            }

			/// <summary>
            /// Deletes a user.
            /// </summary>
            /// <param name="config"></param>
            /// <param name="user"></param>
			public static void Delete(Config config, User user)
            {
                try
                {
                    string uri = config.APIUrl + "users/";
                    uri += user.id.ToString();
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.DELETE);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }                    
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
			
			/// <summary>
            /// Blocks a user. Admin only function
            /// </summary>
            /// <param name="config"></param>
            /// <param name="user"></param>
			public static void Block(Config config, User user)
            {
                try
                {
                    string uri = config.APIUrl + "users/" + user.id.ToString() + "/block";
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.PUT);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            public static void UnBlock(Config config, User user)
            {
                try
                {
                    string uri = config.APIUrl + "users/" + user.id.ToString() + "/unblock";
                    RestClient client = new RestClient(uri)
                    {
                        Timeout = -1
                    };
                    RestRequest request = new RestRequest(Method.PUT);
                    request.AddHeader("Accept", "application/json");
                    request.AddHeader("PRIVATE-TOKEN", config.APIKey);
                    IRestResponse response = client.Execute(request);
                    int responseCode = Convert.ToInt32(response.StatusCode);
                    if (responseCode < 200 || responseCode >= 300)
                    {
                        throw new GitLabServerErrorException(response.Content, responseCode);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }
}